# 不具合・疑問があった場合は

* [課題 · solutus / WordPressDevEnv · GitLab](https://gitlab.com/solutus-llc/wordpressdevenv/-/issues) までご連絡 Issue いただけると幸いです。

# 前提条件

* Docker が必要です。
    - [Install Docker Compose | Docker Documentation](https://docs.docker.com/compose/install/)

* ターミナル/コマンド操作 が発生します。

# 使用法

## 起動

### 新WordPress開発環境を作る場合

1. `docker-compose up -d`

2. 数分待ち `localhost:8080` へアクセスする

* 初期ユーザ(Super Admin)のアカウントは`.env.wordpress` 内の `WORDPRESS_ADMIN_*`にて適宜定義する

### 既存WordPress環境を手元端末で再現する場合

1. 既存WordPressデータベース(*.sql, *.sql.gz)の取得・配置

    a. `database` ディレクトリへ配置 or  
    b. salvage を使った方法
        1. `.env.salvage-db` 内の値を記入する
        2. `docker-compose -f docker-compose.salvage.yml up --force-recreate`
        
2. 既存WordPressファイル群の配置
    - `wordpress` ディレクトリ配下へ配置する
    
3. **新WordPress開発環境を作る場合** を実施する


## 停止・休止

`docker-compose stop`

## 破棄・削除

`docker-compose down`

その他、docker-compose に準ずる
