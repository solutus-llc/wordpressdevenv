#!/bin/bash

if [[ ! -e $PWD/wp-config.php ]]; then
  echo "The current working directory is not the WordPress root directory."
  echo "This script file should be run in the WordPress root directory."
  exit 1;
fi

chown -R 33:33 $PWD/wp-content & chown 33:33 $PWD/wp-config.php

printenv | sed 's/^WP_CONFIG_//;t;d' | sed 's/=/ /' | while read LINE; do wp --allow-root config set $LINE; done

wp --allow-root option update home $WP_HOME

wp --allow-root option update siteurl $WP_SITEURL

wp --allow-root core install \
  --url=$WP_HOME \
  --title=$WP_TITLE \
  --admin_user=$WP_ADMIN_USER \
  --admin_password=$WP_ADMIN_PASSWORD \
  --admin_email=$WP_ADMIN_EMAIL \
  --skip-email

if [[ $WP_SCAFFOLD_UNDERSCORES == true ]]; then

  # `wp --allow-root theme is-installed $UNDERSCORES_THEME_NAME` はディレクトリの有無でインストール済み判定しているので使えない
  if [[ ! -e $PWD/wp-content/themes/$UNDERSCORES_THEME_NAME/style.css ]]; then
    wp --allow-root scaffold underscores $UNDERSCORES_THEME_NAME
  fi

  wp --allow-root theme is-active $UNDERSCORES_THEME_NAME
  if [[ $? != 0 ]]; then
    wp --allow-root theme activate $UNDERSCORES_THEME_NAME
  fi
fi

